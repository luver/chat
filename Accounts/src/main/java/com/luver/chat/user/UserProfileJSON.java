package com.luver.chat.user;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class UserProfileJSON {
    public UserProfileJSON() {
    }

    public UserProfileJSON(UserProfile userProfile, String userName) {
        this.userName = userName;
        name = userProfile.getName();
        age = userProfile.getAge();
        email = userProfile.getEmail();
    }

    private String userName;
    private String name;
    private int age;
    private String email;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
