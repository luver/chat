package com.luver.chat.authentication.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Controller
public class LoginController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(HttpServletRequest request) {
        try {
            Authentication req = new UsernamePasswordAuthenticationToken(request.getParameter("name"),
                    request.getParameter("password"));                     // I think it should be @RequestParam
            Authentication result = authenticationManager.authenticate(req);
            SecurityContextHolder.getContext().setAuthentication(result);
            return "OK";  // With ResponseBody in returns just "200 OK"
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return "500";
        }
    }

}
