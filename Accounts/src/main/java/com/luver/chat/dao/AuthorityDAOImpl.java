package com.luver.chat.dao;

import com.luver.chat.user.Authority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Repository
@Transactional
public class AuthorityDAOImpl extends JpaDaoSupport implements AuthorityDAO {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    public AuthorityDAOImpl(EntityManager entityManager) {
        setEntityManager(entityManager);

    }

    @Override
    public void save(Authority authority) {
        getJpaTemplate().persist(authority);
    }

    @Override
    public void delete(Authority authority) {
        authority = getJpaTemplate().getReference(Authority.class, authority.getId());
        getJpaTemplate().remove(authority);
    }

    @Override
    public void update(Authority authority) {
        getJpaTemplate().merge(authority);
    }

    @Override
    public Authority findById(long id) {
        return getJpaTemplate().find(Authority.class, id);
    }

    @Override
    public Authority findByName(final String name) {
        Query query = entityManager.createQuery("from Authority a where a.authorityName=:name");
        query.setParameter("name", name);
        return (Authority) query.getSingleResult();
    }

    @Override
    public List<Authority> findAll(String name) {
        return getJpaTemplate().find("from " + Authority.class);
    }
}
