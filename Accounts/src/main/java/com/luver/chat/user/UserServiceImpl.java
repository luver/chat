package com.luver.chat.user;

import com.luver.chat.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Repository
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDAO userDAO;

    @Override
    public void createUser(User user, UserProfile userProfile) {
        userDAO.saveUser(user);
        userDAO.saveProfile(userProfile);
    }

    @Override
    public void createUser(User user) {
        userDAO.saveUser(user);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    @Override
    public void updateUser(User user, UserProfile userProfile) {
        userDAO.updateUser(user);
        userDAO.updateProfile(userProfile);
    }

    @Override
    public void updateUserProfile(UserProfile userProfile) {
        userDAO.updateProfile(userProfile);
    }

    @Override
    public void deleteUser(User user) {
       userDAO.deleteUser(user);
    }

    @Override
    public User getUserByName(String name) {
        return userDAO.findUserByName(name);
    }

    @Override
    public User getUserById(long id) {
        return userDAO.findUserById(id);
    }

    @Override
    public UserProfile getUserProfileByName(String name) {
        return userDAO.getProfileByName(name);
    }
}
