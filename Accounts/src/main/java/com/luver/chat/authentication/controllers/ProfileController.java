package com.luver.chat.authentication.controllers;

import com.luver.chat.user.UserProfile;
import com.luver.chat.user.UserProfileJSON;
import com.luver.chat.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Controller

public class ProfileController {
    @Autowired
    private UserService userService;

    @ResponseBody
    @RequestMapping(value = "/profile/{name}", method = RequestMethod.GET)
    public UserProfileJSON getProfile(@PathVariable("name") String username) {
        UserProfile userProfile = userService.getUserProfileByName(username);
        UserProfileJSON userProfileJSON = new UserProfileJSON(userProfile, username);
        return userProfileJSON;
    }

    @ResponseBody
    @RequestMapping(value = "/profile/avatar/{name}", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getAvatar(@PathVariable("name") String username) {
        UserProfile userProfile = userService.getUserProfileByName(username);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(userProfile.getAvatar(), responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/profile/editprofile", method = RequestMethod.GET)
    public String editProfile(ModelMap modelMap) {
        modelMap.addAttribute("name", SecurityContextHolder.getContext().getAuthentication().getName());
        return "profile";
    }

    @RequestMapping(value = "/profile/saveprofile", method = RequestMethod.POST)
    public String saveProfile(@RequestParam String name,
                              @RequestParam String email,
                              @RequestParam String age,
                              Principal principal) {
        Logger logger = Logger.getLogger(ProfileController.class);
        logger.debug("Principal name:" + SecurityContextHolder.getContext().getAuthentication().getName());
        UserProfile userProfile = userService.getUserProfileByName(SecurityContextHolder.getContext().getAuthentication().getName());
        userProfile.setName(name);
        userProfile.setEmail(email);
        userProfile.setAge(Integer.parseInt(age));
        userService.updateUserProfile(userProfile);
        return "index";
    }
}
