package com.luver.chat.dao;

import com.luver.chat.user.Authority;

import java.util.List;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface AuthorityDAO {
    void save(Authority authority);

    void delete(Authority authority);

    void update(Authority authority);

    Authority findById(long id);

    Authority findByName(String name);

    List<Authority> findAll(String name);
}
