package com.luver.chat.authentication;

import com.luver.chat.dao.AuthorityDAO;
import com.luver.chat.dao.UserDAO;
import com.luver.chat.user.Authority;
import com.luver.chat.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AuthorityDAO authorityDAO;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException, DataAccessException {
        final User user = userDAO.findUserByName(name);
        UserDetails details = new UserDetails() {
            @Override
            public Collection<GrantedAuthority> getAuthorities() {
                final Authority authority = authorityDAO.findById(user.getAuthority());
                ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>() {{
                    add(new GrantedAuthority() {
                        @Override
                        public String getAuthority() {
                            return authority.getAuthorityName();
                        }
                    });
                }};
                return grantedAuthorities;
            }

            @Override
            public String getPassword() {
                return user.getPasswordHash();
            }

            @Override
            public String getUsername() {
                return user.getUsername();
            }

            @Override
            public boolean isAccountNonExpired() {
                return user.isEnabled();
            }

            @Override
            public boolean isAccountNonLocked() {
                return user.isEnabled();
            }

            @Override
            public boolean isCredentialsNonExpired() {
                return user.isEnabled();
            }

            @Override
            public boolean isEnabled() {
                return user.isEnabled();
            }
        };
        return details;
    }
}
