package com.luver.chat.user;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface UserService {
    void createUser(User user, UserProfile userProfile);
    void createUser(User user);
    void updateUser(User user);
    void updateUser(User user, UserProfile userProfile);
    void updateUserProfile(UserProfile userProfile);
    void deleteUser(User user);
    User getUserByName(String name);
    User getUserById(long id);
    UserProfile getUserProfileByName(String name);
}
