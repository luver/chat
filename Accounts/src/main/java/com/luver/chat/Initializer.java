package com.luver.chat;

import com.luver.chat.dao.AuthorityDAO;
import com.luver.chat.dao.UserDAO;
import com.luver.chat.user.Authority;
import com.luver.chat.user.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AuthorityDAO authorityDAO;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        Logger logger = Logger.getLogger(Initializer.class);
        logger.debug("onApplicationEvent invoked");
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);

        Authority userAuthority = authorityDAO.findByName("user");
        if (userAuthority == null) {
            userAuthority = new Authority();
            userAuthority.setAuthorityName("user");
            authorityDAO.save(userAuthority);
        } else {
            logger.debug("Authority 'user' already exists. Skip.");
        }

        Authority adminAuthority = authorityDAO.findByName("admin");
        if (adminAuthority == null) {
            adminAuthority = new Authority();
            adminAuthority.setAuthorityName("admin");
            authorityDAO.save(adminAuthority);
        } else {
            logger.debug("Authority 'admin' already exists. Skip.");
        }

        User user = userDAO.findUserByName("user1");
        if (user == null) {
            user = new User();
            user.setAuthority(userAuthority.getId());
            user.setUsername("user1");
            user.setPasswordHash("password"); // md5 hashing doesn't work, smth wrong in applicationContext-security.xml
            user.setEnabled(true);
            userDAO.saveUser(user);
        } else {
            logger.debug("User 'user1' already exists. Skip.");
        }
    }
}
