package com.luver.chat.user;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Entity
public class Authority {
    @Id
    @GeneratedValue
    private long id;

    private String authorityName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName;
    }

}
