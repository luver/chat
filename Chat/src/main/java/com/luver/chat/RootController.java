package com.luver.chat;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@RequestMapping(value = "/")
@Controller
public class RootController {

    @RequestMapping(method = RequestMethod.GET)
    public String doRoot() {
        return "index";
    }

    @RequestMapping(value = "/loginpage", method = RequestMethod.GET)
    public String showLoginPage() {
        return "loginPage";
    }

    @PreAuthorize("hasRole('user')")
//    @Secured("user")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public String user(ModelMap modelMap) {
        modelMap.addAttribute("user",
                SecurityContextHolder.getContext().getAuthentication().getCredentials().toString());
        return "index";
    }
}
