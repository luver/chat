package com.luver.chat.dao;

import com.luver.chat.user.User;
import com.luver.chat.user.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.support.JpaDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
@Repository
@Transactional
public class UserDAOImpl extends JpaDaoSupport implements UserDAO {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    public UserDAOImpl(EntityManager entityManager) {
        setEntityManager(entityManager);
    }

    @Override
    public void saveUser(User user) {
        getJpaTemplate().persist(user);
    }

    @Override
    public void deleteUser(User user) {
        user = getJpaTemplate().getReference(User.class, user.getId());
        getJpaTemplate().remove(user);
    }

    @Override
    public void updateUser(User user) {
        getJpaTemplate().merge(user);
    }

    @Override
    public User findUserById(long id) {
        return getJpaTemplate().find(User.class, id);
    }

    @Override
    public User findUserByName(String name) {
//        Logger logger = Logger.getLogger(UserDAOImpl.class);
//        logger.debug("ACHTUNG! Query for "+name);
        Query query = entityManager.createQuery("from User u where u.username=:username");
        query.setParameter("username", name);
        return (User) query.getSingleResult();
    }

    @Override
    public List<User> findAllUsers() {
        return getJpaTemplate().find("from " + User.class);
    }

    @Override
    public UserProfile getProfileByName(String name) {
        User user = findUserByName(name);
        Query query = entityManager.createQuery("from UserProfile up where up.userId=:id");
        query.setParameter("id", user.getId());
        UserProfile result = (UserProfile) query.getSingleResult();
        return result;
    }

    @Override
    public void updateProfile(UserProfile profile) {
        getJpaTemplate().merge(profile);
    }

    @Override
    public void saveProfile(UserProfile profile) {
        getJpaTemplate().persist(profile);
    }

    @Override
    public void deleteProfile(UserProfile profile) {
        profile = getJpaTemplate().getReference(UserProfile.class, profile.getId());
        getJpaTemplate().remove(profile);
    }
}
