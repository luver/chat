package com.luver.chat.dao;

import com.luver.chat.user.User;
import com.luver.chat.user.UserProfile;

import java.util.List;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface UserDAO {
    void saveUser(User user);

    void deleteUser(User user);

    void updateUser(User user);

    User findUserById(long id);

    User findUserByName(String name);

    List<User> findAllUsers();

    UserProfile getProfileByName(String name);

    void updateProfile(UserProfile profile);
    
    void saveProfile(UserProfile profile);

    void deleteProfile(UserProfile profile);
}
